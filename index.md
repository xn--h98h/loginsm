---
title: Static Login Server
layout: default
---
# Login Server

<meta charset="utf8"/>

a login server is a place where you can authentify against a registry
and therefore it must exist a "logically central place" where your
public identity information is stored

Using static site is rendering the registration process difficult
as you need a place anyone can write to
usually is it made on a central server
or via some forms (goolgesheets or [framacalc][1] or airtable etc.)


[1]: https://lite.framacalc.org/login/form



### login via a static site:


Static websites lack of "mutability" i.e.
a mean to record and update dynamically data from any users to a public namespace.

centralized server are the easy solution for this,
however they rendering the internet links fragile.

In fact naming a location is nothing else
than logically linking a physical place to a mutable address.

And there is no mean to change all instance of "addresses" when 
we moved the information arround. leading to the [404] symptoms.

Machine generated addresses (like [CAFS]) are a solution as they establish
a bijective relationship between places and addresses, however
this make addressing permananent an we loose the ability to
track any mutable information.

One need a central place to name machine generated addresses
* GIT repositories is a way to achieve this "namespace" mapping
* email inbox is an other
* or any system that collect information from anoynmous user
  an act as a "public" [bulletin board][bbs]

Implementation of such system are often centralized, as it 
is difficult to achieve the function in an distributed manner.
as it require to listen on a line (pulling).


All these system as subject to [SPAM] or [DDoS]


<!-- coarkboard, bulletinboard, letterboard,
     chalkboard, whiteboard, blackboard, imageboard -->





We will be using [staticman][sm] to "POST" registration data to git,

alternatives are

*git-gateway (netlify->github/gitlab/bitbucket)



[CAFS]: https://qwant.com/?q=Content+Addressed+FileStore
[bb]: https://qwant.com/?q=Distributed+Bulletinboard+System
[sm]: https://staticman.net/
[SPAM]: {{site.link}}=define+spam
[DDoS]: {{site.link}}=define+DDoS

